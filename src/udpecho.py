#!/usr/bin/env python2
# -*- coding: utf-8 -*-

# Adapted from:

# Author: David Manouchehri <manouchehri@protonmail.com>
# This script will always echo back data on the UDP port of your choice.
# Useful if you want nmap to report a UDP port as "open" instead of "open|filtered" on a standard scan.
# Works with both Python 2 & 3.

import socket
import sys

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

if len(sys.argv) < 2:
    print(f"Usage: {sys.argv} port")
    sys.exit(1)

server_address = '0.0.0.0'
server_port = int(sys.argv[1])

server = (server_address, server_port)
sock.bind(server)
print("Listening on " + server_address + ":" + str(server_port))

while True:
    payload, client_address = sock.recvfrom(1)
    print("Echoing data back to " + str(client_address))
    sent = sock.sendto(payload, client_address)
