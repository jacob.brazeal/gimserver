const UDPPort = 9513;

class Cluster extends Widget {
    constructor(selector) {
        super('cluster', selector);

        // Fetch peers. Then fetch data.

        this.state.ready = 0;

        this.state.dataByIp = {};
        this.state.neededDataCount = 0;
        this.state.haveDataCount = 0;

        this.state.ip = $('#ip').val();
        this.state.name = $('#name').val();

        this.state.since = 0;
        this.state.window = localStorage.gimWindow ? parseInt(localStorage.gimWindow) : 30 * 60;

        this.state.analysisType = localStorage.gimAnalysisType || 'SampleCountHeatmap';

        this.state.lastUpdated = 0;

        this.state.isLocal = this.state.ip.includes('localhost');

        this.fetchData();
    }

    fetchData() {
        fetch('/api/peers').then(x => x.json()).then(res => {

            // Ensure that we're present exactly once in the list, regardless of whether we're self-peered
            this.state.peers = Object.fromEntries(res.filter(x => x.to_ip !== this.state.ip).concat([
                {
                    "name": this.state.name,
                    "to_ip": window.location.hostname,
                    "to_port": window.location.port
                }
            ]).map((peer) => [peer.to_ip, peer]));
            this.state.peerList = Object.values(this.state.peers).sort((a, b) => a.to_ip < b.to_ip ? -1 : 1)

            this.state.dataByIp = Object.fromEntries(this.state.peerList.map(p => [
                p.to_ip,
                Object.fromEntries(this.state.peerList.map(p2 => [
                    p2.to_ip,
                    []
                ]))
            ]))

            this.state.neededDataCount = this.state.peerList.length * (this.state.peerList.length);
            if (this.state.isLocal) {
                // this.state.neededDataCount -= this.state.peers.length; // No inbound link from external peers and no self-peer
            }

            this.setState({
                ready: 1
            })


            this.state.fetcher = this.state.peerList.flatMap(peer => this.state.peerList.map(peer2 => fetch(
                `http://${peer.to_ip}:${peer.to_port}/api/perf/${peer2.to_ip}/${UDPPort}?since=${this.state.since}&&window=${this.state.window}`
            ).then(x => x.json()).then(data => {
                this.state.dataByIp[peer.to_ip] = this.state.dataByIp[peer.to_ip] || {};
                this.state.dataByIp[peer.to_ip][peer2.to_ip] = data.data;
            }).catch(e => {
                console.error(e);
                this.state.dataByIp[peer.to_ip] = this.state.dataByIp[peer.to_ip] || {};
                this.state.dataByIp[peer.to_ip][peer2.to_ip] = [];

                return {
                    peer1: peer.to_ip,
                    peer2: peer2.to_ip,
                    error: true
                }
            }).finally(() => {
                this.setState({
                    haveDataCount: this.state.haveDataCount + 1
                })
            })))

            return Promise.all(this.state.fetcher).then(() => {

                this.state.lastUpdated = new Date().getTime();
                this.setState({
                    ready: 2
                })
            })

        }).catch(e => {
            console.error(e);
            this.setState({
                error: true
            })
        })
    }

    updateWindow(win) {
        win = parseInt(win);
        localStorage.gimWindow = win;
        window.location.reload();
    }

    updateAnalysis(analysisType) {
        localStorage.gimAnalysisType = this.state.analysisType = analysisType;
        this.mount();
    }

    mixArrays(a, b, p) {
        return a.map((x, i) => (1 - p) * x + p * b[i]);
    }

    constructHeatmap(valueTable, transformFunc, renderFunc, labelFunc, color1, color2) {

        const allDataSets = Object.values(valueTable).flatMap(x => Object.values(x));

        const allSampleCounts = allDataSets.map(x => transformFunc(x));

        const min = Math.min.apply(null, allSampleCounts),
            max = Math.max.apply(null, allSampleCounts);

        let html = '';
        html += `<table class="heatmap" >`;
        // Header row
        const colLabels = Object.keys(Object.values(valueTable)[0]);

        html += '</tr>';
        html += `<td></td>`;
        for (const label of colLabels) {
            html += `<td>${labelFunc(label)}</td>`;
        }
        html += '</tr>'

        let index = 0;

        for (const [label, row] of Object.entries(valueTable)) {
            html += '<tr>';
            html += '<td>' + labelFunc(label) + '</td>';
            for (const [colLabel, cell] of Object.entries(row)) {
                const value = transformFunc(cell);
                const display = renderFunc(cell);

                const percent = (value - min) / (max - min);
                const color = `rgb(${this.mixArrays(color1, color2, percent).map(x => x.toFixed(0)).join(',')})`;
                html += `<td style="background: ${color}">${display}</td>`
            }
            html += '</tr>';
            ++index;
        }


        html += '</table>';
        return html;
    }

    pearsonCorrelation(d1, d2) {
        /**
         * calculates pearson correlation
         * @param {number[]} d1
         * @param {number[]} d2
         */
        let {min, pow, sqrt} = Math
        let add = (a, b) => a + b
        let n = min(d1.length, d2.length)
        if (n === 0) {
            return 0
        }
        [d1, d2] = [d1.slice(0, n), d2.slice(0, n)]
        let [sum1, sum2] = [d1, d2].map(l => l.reduce(add))
        let [pow1, pow2] = [d1, d2].map(l => l.reduce((a, b) => a + pow(b, 2), 0))
        let mulSum = d1.map((n, i) => n * d2[i]).reduce(add)
        let dense = sqrt((pow1 - pow(sum1, 2) / n) * (pow2 - pow(sum2, 2) / n))
        if (dense === 0) {
            return 0
        }
        return (mulSum - (sum1 * sum2 / n)) / dense

    }

    getNodeTimeseries(node = this.state.ip, incoming = true) {
        const result = {};

        const timestampToBucket = timestamp => timestamp - (timestamp % this.state.window);

        const byTime = {};

        let timeseriesByIp = {};

        const covarianceMatrix = {};

        // 1) Filter and invert data if necessary
        if (incoming) {
            for (const [from, targets] of Object.entries(this.state.dataByIp)) {
                timeseriesByIp[from] = targets[node] || [];
            }
        } else {
            timeseriesByIp = this.state.dataByIp[node];
        }

        // 2) Bin by time
        for (const [peer, timeseries] of Object.entries(timeseriesByIp)) {
            for (const point of timeseries) {
                const timestamp = timestampToBucket(point.gap);
                byTime[timestamp] = byTime[timestamp] || {};
                byTime[timestamp][peer] = point.rtt;
            }

        }

        // 3) Construct covariance matrix
        const delta = arr => {
            if (arr.length === 0) return [];
            const avg = arr.reduce((a, b) => a + b) / arr.length;
            const deltas = arr.map(x => x - avg);
            return deltas;
        }
        for (const peer of Object.keys(timeseriesByIp)) {
            covarianceMatrix[peer] = {};
            for (const peer2 of Object.keys(timeseriesByIp)) {
                const arr1 = [], arr2 = [];
                let stats = covarianceMatrix[peer][peer2] = {}
                for (const [timestamp, bin] of Object.entries(byTime)) {
                    if (peer in bin && peer2 in bin) {
                        arr1.push(bin[peer]);
                        arr2.push(bin[peer2]);
                    }
                }
                stats.correlation = this.pearsonCorrelation(arr1, arr2);
                console.log(peer, peer2, arr1.slice(0, 10), arr2.slice(0, 10), delta(arr1).slice(0, 10), delta(arr2).slice(0, 10));
                stats.deltaCorrelation = this.pearsonCorrelation(delta(arr1), delta(arr2));
                stats.samples = arr1.length;
            }
        }
        return covarianceMatrix;
    }

    renderAnalysis() {
        let html = '';

        if (this.state.analysisType === 'SampleCountHeatmap') {
            html += this.constructHeatmap(this.state.dataByIp, (x => x.length), (x => x.length), label => this.state.peers[label].name, [255, 255, 255], [0, 0, 255]);
        } else if (this.state.analysisType === 'StdDevMap') {
            function stddev(array) {
                if (array.length < 2) return 0;
                // https://stackoverflow.com/questions/7343890/standard-deviation-javascript
                const n = array.length;
                const mean = array.reduce((a, b) => a + b) / n;
                return Math.sqrt(array.map(x => Math.pow(x - mean, 2)).reduce((a, b) => a + b) / n);
            }

            html += this.constructHeatmap(this.state.dataByIp, (x => stddev(x.map(y => y.rtt))), (x => stddev(x.map(y => y.rtt)).toFixed(2)), label => this.state.peers[label].name, [255, 255, 255], [255, 0, 0]);
        } else if (this.state.analysisType === 'RTTHeatmap') {
            function median(array) {
                const sorted = array.slice().sort();
                if (sorted.length < 1) return 0;
                if ((sorted.length & 1)) {
                    return sorted[Math.floor(sorted.length / 2)]
                } else {
                    return (sorted[Math.floor(sorted.length / 2)] + sorted[Math.floor(sorted.length / 2) + 1]) / 2;
                }
            }

            html += this.constructHeatmap(this.state.dataByIp, (x => median(x.map(y => y.rtt))), (x => x.length ? median(x.map(y => y.rtt)).toFixed(2) : '-'), label => this.state.peers[label].name, [255, 255, 255], [255, 0, 0]);
        } else if (this.state.analysisType === 'IncomingLatencyCorrelation') {
            const incomingTimeseries = this.getNodeTimeseries(this.state.ip, true)
            html += this.constructHeatmap(incomingTimeseries, (x => x.correlation), (x => `${(100 * x.correlation).toFixed(2)} (${x.samples})`), label => this.state.peers[label].name, [255, 255, 255], [0, 255, 0]);
        } else if (this.state.analysisType === 'OutgoingLatencyCorrelation') {
            const incomingTimeseries = this.getNodeTimeseries(this.state.ip, false)
            html += this.constructHeatmap(incomingTimeseries, (x => x.correlation), (x => `${(100 * x.correlation).toFixed(2)} (${x.samples})`), label => this.state.peers[label].name, [255, 255, 255], [0, 255, 0]);
        } else if (this.state.analysisType === 'Lag1AutoCorr') {
            const lag1correlation = (array) => {
                if (array.length < 2) return 0;
                return this.pearsonCorrelation(array.slice(0, -1), array.slice(1))
            }

            html += this.constructHeatmap(this.state.dataByIp, (x => lag1correlation(x.map(y => y.rtt))), (x => lag1correlation(x.map(y => y.rtt)).toFixed(2)), label => this.state.peers[label].name, [255, 255, 255], [0, 255, 0]);
        }

        // else if (this.state.analysisType === 'IncomingLatencyDeltaCorrelation') {
        //     const incomingTimeseries = this.getNodeTimeseries(this.state.ip, false)
        //     html += this.constructHeatmap(incomingTimeseries, (x => x.deltaCorrelation), (x => `${(100 * x.correlation).toFixed(2)} (${x.samples})`), label => this.state.peers[label].name, [255, 255, 255], [0, 255, 0]);
        // } else if (this.state.analysisType === 'OutgoingLatencyDeltaCorrelation') {
        //     const incomingTimeseries = this.getNodeTimeseries(this.state.ip, false)
        //     html += this.constructHeatmap(incomingTimeseries, (x => x.deltaCorrelation), (x => `${(100 * x.correlation).toFixed(2)} (${x.samples})`), label => this.state.peers[label].name, [255, 255, 255], [0, 255, 0]);
        // }

        return html;
    }

    render() {
        if (this.state.ready === 0) {
            return 'Fetching list of peers...';
        }

        if (this.state.ready === 1) {
            return `Loading data from cluster: ${(100 * this.state.haveDataCount / this.state.neededDataCount).toFixed(0)}% complete...`
        }
        let html = '';

        html += `<div>Last updated: ${new Date(this.state.lastUpdated).toLocaleString()}</div>`

        html += `<div class="cluster-ctrls" ><select onchange="this.root.updateAnalysis(this.value)" class="analysisType" >
       ${Object.entries({
            'SampleCountHeatmap': "Sample Count",
            'StdDevMap': "Latency StdDev",
            'RTTHeatmap': "RTT Median (ms)",
            'IncomingLatencyCorrelation': "Incoming Latency Correlation ",
            'OutgoingLatencyCorrelation': "Outgoing Latency Correlation ",
            'Lag1AutoCorr': "Lag-1 Autocorrelation",
            // 'IncomingLatencyDeltaCorrelation': "Incoming Latency Delta Correlation Heatmap",
            // 'OutgoingLatencyDeltaCorrelation': "Outgoing Latency Delta Correlation Heatmap",
        }).map(([code, display]) => `<option ${code === this.state.analysisType ? 'selected' : ''}
value="${code}" >${display}</option>`)} 
</select>

            <select onchange="this.root.updateWindow(this.value)" class="timespan" >
               ${Object.entries({
            5: "5 second window",
            60: "60 second window",
            [60 * 5]: "5 minute window",
            [60 * 10]: "10 minute window",
            [60 * 30]: "30 minute window",
            [60 * 60]: "60 minute window"
        }).map(([window, title]) => `<option ${parseInt(window) === this.state.window ? 'selected' : ''}
 value="${window}" >${title}</option>`)} 
            </select>

</div>
`
        if (this.state.error) html += '<div>There was an error</div>';

        else html += this.renderAnalysis()


        return html;
    }
}
