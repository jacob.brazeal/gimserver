class Widget {
    constructor(type, selector) {
        this.type = type;

        this.className = `${type}--${Math.random().toString(16).slice(2)}`
        this.selector = selector || '.' + this.className;

        this.cmp = {};
        this.state = {};
    }

    get parent() {
        return $(this.selector)
    }

    render() {
        return '';
    }

    setState(props) {
        this.state = Object.assign(this.state, props);
        this.mount();
    }

    mount() {

        const root = this.parent.get(0)
        root.innerHTML = this.render();

         const setRoot = (el)=> {
            for (const child of [...el.children]) {
                child.root = this;
                setRoot(child);
            }
        }

        setRoot(root);

        this.mountComponents();

    }

    mountComponents() {
        if (this.state.ready === false) return;
        for (const cmp of Object.values(this.cmp)) {
            cmp.mount();
        }
    }
}
