function addTarget() {
    const ip = $('.ta__ip').val();
    const port = $('.ta__port').val();
    const reqType = $('.ta__req-type').val();
    const name = $('.ta__name').val();
    fetch(`/api/target/${ip}/${port}`, {
        method: 'POST',
        body: JSON.stringify({
            name, reqType
        }),
        headers: new Headers({
            'content-type': 'application/json'
        })
    }).then(x => x.json())
        .then(res => {
            if (res.ok) {
                window.location.reload();
            } else {

                throw res;
            }
        }).catch(e => {
        console.error(e);
        alert('There was a problem acquiring the target');
    })

}

function deleteTarget(ip, port) {
    if (!confirm("Are you sure you want to delete this target? You will lose access to the data you've collected.")) {
        return;
    }

    fetch(`/api/target/${ip}/${port}`, {
        method: 'POST',
        body: JSON.stringify({
            delete: true
        }),
        headers: new Headers({
            'content-type': 'application/json'
        })

    }).then(x => x.json())
        .then(res => {
            if (res.ok) {
                window.location.reload();
            } else {
                throw res;
            }
        }).catch(e => {
        console.error(e);
        alert('There was a problem deleting the target');
    })
}
