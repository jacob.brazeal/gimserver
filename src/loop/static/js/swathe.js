class Swathe extends Widget {
    constructor(selector) {
        super('swathe', selector);

        this.state.window = localStorage.gimWindow ? parseInt(localStorage.gimWindow) : 30 * 60;
        this.state.since = 0;
        this.state.targets = [];
        this.state.stats = {}

        this.state.ready = false;
        // [this.state.ip, this.state.port] = window.location.pathname.split('/').slice(2);
        this.state.ip = $('#ip').val();

        Promise.all([
            fetch(`api/targets`).then(x => x.json()).then(targets=> {
                this.state.targets = targets;
            })
        ]).then(() => {
            this.scheduleLoop();

            this.setState({
                ready: true
            })
        }).catch(e => {
            console.error(e);
            this.setState({
                err: true
            })
        })

    }

    scheduleLoop() {
        if (this.statFetcher) {
            clearInterval(this.statFetcher);
        }
        this.statFetcher = setInterval(this.fetchLiveStats.bind(this), this.state.window * 1000);
        this.fetchLiveStats(true);
    }

    fetchLiveStats(force) {
        if (document.hidden && !force) {
            return; // Don't request if not in foreground
        }

        let sources = this.state.targets.map(target => fetch(`/api/perf/${target.to_ip}/${target.to_port}?since=${this.state.since}&&window=${this.state.window}`)
            .then( x=> x.json()))


        return Promise.all(sources)
            .then(res => {
                const stats = Object.fromEntries(res.map((x, idx) => [this.state.targets[idx].name, x.data]));
                if (JSON.stringify(stats) === JSON.stringify(this.state.stats)) {
                    return;
                }
                this.setState({
                    stats: stats
                })
            })
    }

    updateWindow(window) {
        window = parseInt(window);
        localStorage.gimWindow = this.state.window = window;
        this.scheduleLoop();
    }

    render() {
        if (this.state.err) return 'There was a problem';
        if (!this.state.ready || !Object.keys(this.state.stats).length) return 'Loading...';

        let html = `<div class="target-wrap">
            <div>Monitoring from: ${this.state.ip}</div>
            <hr>
            <select onchange="this.root.updateWindow(this.value)" class="timespan" >
               ${Object.entries({
            5: "5 second window",
            60: "60 second window",
            [60 * 5]: "5 minute window",
            [60 * 10]: "10 minute window",
            [60 * 30]: "30 minute window",
            [60 * 60]: "60 minute window"
        }).map(([window, title]) => `<option ${parseInt(window) === this.state.window ? 'selected' : ''}
 value="${window}" >${title}</option>`)} 
            </select>
            
            <canvas width="400" height="400" class="graph-wrap" ></canvas>
        </div>`;

        return html;

    }

    mountGraph() {
        const colors = [
            'rgb(93,165,218)',
            'rgb(96,189,104)',
            'rgb(250,164,58)',
            'rgb(241,88,84)',
            'rgb(178,118,178)',
        ]

        if (!this.state.ready) return;
        const ctx = $('.graph-wrap', this.parent).get(0);
        this.chart = new Chart(ctx, {
            type: 'scatter',
            data: {
                datasets: Object.entries(this.state.stats).map(([label, points], index) => ({
                    data: points.map(x => ({x: x.start_interval * 1000, y: x.rtt})),
                    label: label,
                    fill: false,
                    showLine: true,
                    borderColor: colors[index % colors.length],
                    tension: 0.1


                }))
            },
            options: {
                scales: {
                    x: {
                        type: 'time',
                    }
                },
                tooltips: {
                    callbacks: {
                        label: function (tooltipItem, data) {
                            var dataset = data.datasets[tooltipItem.datasetIndex];
                            var index = tooltipItem.index;
                            return dataset.labels[index] + ': ' + dataset.data[index];
                        }
                    }
                }

            }

        })
    }

    mountComponents() {
        if (this.state.ready) {
            this.mountGraph();
        }
    }
}

