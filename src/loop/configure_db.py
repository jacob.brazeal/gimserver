import sqlite3, os, csv, sys

cur_directory = os.path.abspath(os.path.dirname(__file__))

con = sqlite3.connect(os.path.join(os.path.abspath(os.path.dirname(__file__)), '../../gim.db'))

cur = con.cursor()

# See if we're already set up?
cur.execute(''' SELECT count(name) FROM sqlite_master WHERE type='table' AND name='pings' ''')
if cur.fetchone()[0] == 1:
    print('Pings table already initialized, skipping.')
else:
    print('Init pings table')
    # Create table
    cur.execute('''CREATE TABLE pings
                   (from_ip text, to_ip text, to_port integer, req_type text, rtt real, timestamp real)''')

# See if we're already set up?
cur.execute(''' SELECT count(name) FROM sqlite_master WHERE type='table' AND name='targets' ''')
if cur.fetchone()[0] == 1:
    print('Targets table already initialized, skipping.')
else:
    print('Init targets table')
    cur.execute('''CREATE TABLE targets
                   (name text, to_ip text, to_port integer, req_type text,
                   PRIMARY KEY (to_ip, to_port)
                   )''')

    # Initialize targets with contents of `seed_ips.tsv`
    with open(os.path.join(cur_directory, "seed_ips.tsv"), "r") as f:
        rd = csv.DictReader(f, delimiter="\t", quotechar='"')
        table = [{k: v for k, v in row.items()}
                 for row in rd]

    for entry in table:
        cur.execute(f"INSERT INTO targets VALUES ('{entry['name']}','{entry['ip']}',{entry['port']},'{entry['type']}')")

# See if we're already set up?
cur.execute(''' SELECT count(name) FROM sqlite_master WHERE type='table' AND name='peers' ''')
if cur.fetchone()[0] == 1:
    print('Peers table already initialized, skipping.')
else:
    print('Init peers table')
    cur.execute('''CREATE TABLE peers
                   (name text, to_ip text, to_port integer, has_acked integer,
                    PRIMARY KEY (to_ip, to_port)
                   )''')

    # Initialize targets with contents of `seed_ips.tsv`
    with open(os.path.join(cur_directory, "seed_peers.tsv"), "r") as f:
        rd = csv.DictReader(f, delimiter="\t", quotechar='"')
        table = [{k: v for k, v in row.items()}
                 for row in rd]

    for entry in table:
        cur.execute(f"INSERT INTO peers VALUES ('{entry['name']}','{entry['ip']}',{entry['port']},0)")

# Save (commit) the changes
con.commit()

# We can also close the connection if we are done with it.
# Just be sure any changes have been committed or they will be lost.
con.close()
