#!/bin/bash

cd "$(dirname "$0")"

export FLASK_ENV="$1"
flask run
