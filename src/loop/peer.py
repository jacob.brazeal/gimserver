import time, csv, os, sys, subprocess, re
import sqlite3, os
import requests

import socket
import traceback

from random import choice

our_ip = socket.gethostbyname(socket.gethostname())

cur_directory = os.path.abspath(os.path.dirname(__file__))
con = sqlite3.connect(os.path.join(cur_directory, '../../gim.db'))
con.row_factory = sqlite3.Row
cur = con.cursor()

MAX_PEERS = 5

try:
    with open('../name.txt', 'r') as idfile:
        ip, name = idfile.read().split()
        port = 80
except:
    ip, name, port = None, None, None  # We're probably a local run, so we don't have an identity to share.
    # In this case, we can't really peer, but we can query the APIs

LOOP_PAUSE = 5

hping = ['sudo', 'hping']


def query_peer_for_data(peer_ip, peer_port):
    print(f"Syncing downstream from {peer_ip}:{peer_port}")
    try:
        # 0 Get out own list of peer data
        our_peers = requests.get('http://localhost:5000/api/peers').json()
        our_targets = requests.get('http://localhost:5000/api/targets').json()

        # 1) Get peer info
        their_peers = requests.get(f'http://{peer_ip}:{peer_port}/api/peers').json()
        print("List of peers received from peer: ", their_peers)

        new_peers = []
        for peer in their_peers:
            if peer['to_ip'] == ip and peer['to_port'] == port:
                continue  # It's us

            is_new = True
            for opeer in our_peers:
                if opeer['to_ip'] == peer['to_ip'] and opeer['to_port'] == peer['to_port']:
                    is_new = False
                    break

            if is_new:
                new_peers.append(peer)

        if len(new_peers):
            print("Adding peers: ", new_peers)
            requests.post('http://localhost:5000/api/peers', json={'peers': new_peers})

        # 2) Get target info
        their_targets = requests.get(f'http://{peer_ip}:{peer_port}/api/targets').json()
        print("List of targets received from peer: ", their_targets)

        new_targets = []
        for target in their_targets:
            if target['to_ip'] == ip and target['to_port'] == port:
                continue  # It's us

            is_new = True
            for otarget in our_targets:
                if otarget['to_ip'] == target['to_ip'] and otarget['to_port'] == target['to_port']:
                    is_new = False
                    break

            if is_new:
                new_targets.append(target)

        if len(new_targets):
            print("Adding targets: ", new_targets)
            requests.post('http://localhost:5000/api/targets', json={'targets': new_targets})

    except Exception as e:
        traceback.print_exc()


def announce_to_peer(peer_ip, peer_port):
    try:
        # First update the status of the connection
        cur.execute(f"UPDATE peers SET has_acked=-2 where to_ip='{peer_ip}' and to_port='{peer_port}'")
        con.commit()

        requests.post(f'http://{peer_ip}:{peer_port}/api/incoming_peer/{ip}/{port}', json={
            'name': name
        })
    except Exception as e:
        traceback.print_exc()


def loop():
    while True:
        # 1) open peers table
        peers = requests.get('http://localhost:5000/api/peers').json()
        print("Current peers: ", peers)
        peers = [peer for peer in peers if
                 not (peer['to_ip'] == ip and peer['to_port'] == port)]  # don't peer with ourselves.

        # 2) peer requests (helping other servers discover us)
        for peer in peers:
            if peer['has_acked'] in [0, -1]:  # We know about them, but no signs that they know about us yet
                if ip is not None:  # If we have an identity, announce
                    announce_to_peer(peer['to_ip'], peer['to_port'])
            elif peer['has_acked'] == -2:  # Waiting for a response
                pass
            elif peer['has_acked'] == 1:  # We're peered!
                pass

        # 3) Ask a random peer for his peers & targets
        if len(peers) > 0:
            random_peer = choice(peers)
            query_peer_for_data(random_peer['to_ip'], random_peer['to_port'])

        # 4) Wait
        time.sleep(LOOP_PAUSE)


if __name__ == '__main__':
    loop()
