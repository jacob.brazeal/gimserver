from flask import Flask, render_template, request, jsonify
import sqlite3, requests
import traceback
import os

app = Flask(__name__)

from flask_cors import CORS, cross_origin

cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

try:
    with open(os.path.abspath(os.path.join(os.path.dirname(__file__), '../name.txt')), 'r') as identity_file:
        global_ip, global_name = identity_file.read().split()
except Exception as e:
    print("No identity file found")
    global_name, global_ip = "Local", "localhost"

def get_db_connection():
    conn = sqlite3.connect('../../gim.db')
    conn.row_factory = sqlite3.Row
    return conn


def get_stats(ip, port, since, window):
    conn = get_db_connection()
    perf = conn.execute(f'''
    SELECT avg(rtt) rtt, round(timestamp) + ({window} - (round(timestamp)%{window})) as gap, count(timestamp),
    min(timestamp) start_interval, max(timestamp) end_interval
    FROM pings 
    WHERE timestamp >= {since} and to_ip='{ip}' and to_port='{port}'
    GROUP BY gap

    ''').fetchall()
    conn.close()
    return perf


def get_all_targets():
    conn = get_db_connection()

    targets = conn.execute('SELECT * FROM targets').fetchall()
    conn.close()
    return targets


def get_all_peers():
    conn = get_db_connection()

    targets = conn.execute('SELECT * FROM peers').fetchall()
    conn.close()
    return targets


def insert_or_ignore_peers(peers):
    print("Adding peers: ", peers)
    conn = get_db_connection()
    cur = conn.cursor()

    tuples = ','.join([f"('{peer['name']}','{peer['to_ip']}',{peer['to_port']}, {0})" for peer in peers])
    cur.execute(f"INSERT OR IGNORE INTO peers(name, to_ip, to_port, has_acked) VALUES {tuples}")
    conn.commit()
    conn.close()


def insert_or_ignore_targets(targets):
    print("Adding targets: ", targets)
    conn = get_db_connection()
    cur = conn.cursor()

    tuples = ','.join(
        [f"('{target['to_ip']}',{target['to_port']}, '{target['name']}', '{target['req_type']}')" for target in
         targets])
    cur.execute(f"INSERT OR IGNORE INTO targets(to_ip, to_port, name, req_type) VALUES {tuples}")
    conn.commit()
    conn.close()


def accept_or_ignore_peer(ip, port, name):
    conn = get_db_connection()
    cur = conn.cursor()

    # Does it already exist>
    match = conn.execute(f"SELECT * FROM peers where to_ip='{ip}' and to_port={port}").fetchall()
    if len(match) > 0:
        return False

    # Add value
    cur.execute(f"INSERT OR IGNORE INTO peers(name, to_ip, to_port, has_acked) VALUES ('{name}', '{ip}',{port}, 1)")
    conn.commit()
    conn.close()

    # Tell peer about it
    requests.post(f'http://{ip}:{port}/api/receive_peer_ack/{ip}/{port}')

    return True


def receive_peer_ack(ip, port):
    conn = get_db_connection()
    cur = conn.cursor()
    cur.execute(f"UPDATE peers set has_acked=1 where ip='{ip}' and port={port}")
    conn.commit()
    conn.close()


def add_target(ip, port, name, req_type):
    print("Adding target: ", ip, port, name, req_type)
    conn = get_db_connection()
    cur = conn.cursor()
    cur.execute(f"INSERT INTO targets VALUES ('{name}','{ip}',{port},'{req_type}')")
    conn.commit()
    conn.close()


def delete_target(ip, port):
    print("Deleting target: ", ip, port)
    conn = get_db_connection()
    cur = conn.cursor()
    cur.execute(f"DELETE FROM targets WHERE to_ip='{ip}' and to_port={port}")
    conn.commit()
    conn.close()


def get_targets(ip, port):
    conn = get_db_connection()
    targets = conn.execute(f"SELECT * FROM targets where to_ip='{ip}' and to_port={port}").fetchall()
    conn.close()
    return targets


@app.route('/')
def index():
    targets = get_all_targets()
    return render_template('index.html', targets=targets, global_ip=global_ip, global_name=global_name)


@app.route('/peers')
def peers():
    peers = get_all_peers()
    return render_template('peers.html', peers=peers, global_ip=global_ip, global_name=global_name)


@app.route('/api/targets', methods=['GET', 'POST'])
def api_targets():
    if request.method == 'GET':
        targets = get_all_targets()
        targets = [{key: row[key] for key in row.keys()} for row in targets]
        return jsonify(targets)
    elif request.method == 'POST':
        try:
            insert_or_ignore_targets(request.json['targets'])
            return jsonify({
                'ok': True
            })
        except Exception as e:
            traceback.print_exc()
            return jsonify({
                'ok': False
            })


@app.route('/api/peers', methods=['GET', 'POST'])
def api_peers():
    if request.method == 'GET':
        peers = get_all_peers()
        peers = [{key: row[key] for key in row.keys()} for row in peers]
        return jsonify(peers)
    elif request.method == 'POST':
        try:
            insert_or_ignore_peers(request.json['peers'])
            return jsonify({
                'ok': True
            })
        except Exception as e:
            traceback.print_exc()
            return jsonify({
                'ok': False
            })


@app.route('/api/incoming_peer/<ip>/<port>', methods=['POST'])
def api_incoming_peers(ip, port):
    if request.method == 'POST':
        try:
            did_work = accept_or_ignore_peer(ip, port, request.json['name'])
            return jsonify({
                'ok': did_work
            })
        except Exception as e:
            traceback.print_exc()
            return jsonify({
                'ok': False
            })


@app.route('/api/receive_peer_ack/<ip>/<port>', methods=['GET'])
def api_receive_peer_ack(ip, port):
    if request.method == 'GET':
        try:
            receive_peer_ack(ip, port)
            return jsonify({
                'ok': True
            })
        except Exception as e:
            traceback.print_exc()
            return jsonify({
                'ok': False
            })


@app.route('/api/target/<ip>/<port>', methods=['GET', 'POST'])
def api_target(ip, port):
    if request.method == 'GET':
        targets = get_targets(ip, port)
        targets = [{key: row[key] for key in row.keys()} for row in targets]
        return jsonify(targets)
    elif request.method == 'POST' and request.json and 'name' in request.json:
        try:
            add_target(ip=ip, port=port, name=request.json['name'], req_type=request.json['reqType'])
            return jsonify({
                'ok': True
            })
        except Exception as e:
            traceback.print_exc()
            return jsonify({
                'ok': False
            })
    elif request.method == 'POST' and request.json and 'delete' in request.json:
        print('Is it true I\'m an eagle?')
        try:
            delete_target(ip=ip, port=port)
            return jsonify({
                'ok': True
            })
        except Exception as e:
            traceback.print_exc()
            return jsonify({
                'ok': False
            })


@app.route('/target/<ip>/<port>')
def target(ip, port):
    return render_template('detail.html', global_ip=global_ip, global_name=global_name)

@app.route('/cluster')
def cluster():
    return render_template('cluster-viz.html', global_ip=global_ip, global_name=global_name)

@app.route('/swathe')
def swathe():
    return render_template('swathe.html', global_ip=global_ip, global_name=global_name)


@cross_origin()
@app.route('/api/perf/<ip>/<port>')
def perf(ip, port):
    since = request.args.get('since', 0)
    window = request.args.get('window', 60 * 15)
    data = get_stats(ip, port, since, window)
    return jsonify({
        'ip': ip,
        'port': port,
        'data': [{key: row[key] for key in row.keys()} for row in data]
    })
