import time, csv, os, sys, subprocess, re
import sqlite3, os
import requests

import socket

our_ip = socket.gethostbyname(socket.gethostname())

cur_directory = os.path.abspath(os.path.dirname(__file__))
con = sqlite3.connect(os.path.join(cur_directory, '../../gim.db'))
cur = con.cursor()

LOOP_PAUSE = 5

hping = ['sudo', 'hping']


def log_request(from_ip, to_ip, to_port, req_type, rtt, timestamp):
    query = f"INSERT INTO pings VALUES ('{from_ip}','{to_ip}',{to_port},'{req_type}', {rtt}, {timestamp})"
    cur.execute(query)
    print(query)
    # Save (commit) the changes
    con.commit()


def extract_hping_rtt_stats(hping_output):
    min, avg, max = map(float,
                        re.search("min/avg/max = ([\d.])+/([\d.])+/([\d.])+", hping_output)[0].split(' = ')[1].split(
                            '/'))
    return {'min': min, 'avg': avg, 'max': max}


def extract_ping_rtt_stats(ping_output):
    min, avg, max, stddev = map(float,
                                re.search("min/avg/max/(stddev|mdev) = ([\d.])+/([\d.])+/([\d.])+/([\d.])+", ping_output)[
                                    0].split(' = ')[1].split(
                                    '/'))
    return {'min': min, 'avg': avg, 'max': max, 'stddev': stddev}


def handle(name, ip, port, type):
    try:
        type = str(type).upper()
        print("Handling target", name, ip, port, type)

        if type == 'UDP':
            # Ping UDP
            when = time.time()
            result = subprocess.run(hping + ["-2", ip, "-p", str(port), "-c", "1"], stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE)
            # print("STDOUT\n\n", result.stdout.decode('utf8'), "\n________________")
            # print("STDERR\n\n", result.stderr.decode('utf8'), "\n________________")

            stats = extract_hping_rtt_stats(result.stderr.decode("utf8"))
            print(stats)

            log_request(our_ip, ip, port, type, stats['avg'], when)
        elif type == 'ICMP':
            # Ping UDP
            when = time.time()
            result = subprocess.run(["ping", ip, "-c", "1"], stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE)
            # print("STDOUT\n\n", result.stdout.decode('utf8'), "\n________________")
            # print("STDERR\n\n", result.stderr.decode('utf8'), "\n________________")

            stats = extract_ping_rtt_stats(result.stdout.decode("utf8"))
            print(stats)

            log_request(our_ip, ip, port, type, stats['avg'], when)
        else:
            raise ValueError("Type not recognized: ", type)

    except Exception as e:
        # Something in handling failed
        print(e)


def loop():
    while True:
        # 1) open IP table
        targets = requests.get('http://localhost:5000/api/targets').json()

        # 2) handle entries
        for entry in targets:
            handle(name=entry['name'], ip=entry['to_ip'], port=entry['to_port'], type=entry['req_type'])

        # 3) Wait
        time.sleep(LOOP_PAUSE)


if __name__ == '__main__':
    loop()
