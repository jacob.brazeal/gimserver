from random import random, seed
from math import floor

seed("HappySummerOff")

iters = 100000
max_connected_nodes = 25
delay = 1

for connected_nodes in range(1, max_connected_nodes):
    total_propagation_steps = 0
    minprop = 999
    maxprop = 0
    for _ in range(iters):
        steps = 1
        known = {0}

        while len(known) < connected_nodes:
            next_known = set()
            steps += 1
            for n in range(connected_nodes):
                if n in known:
                    continue
                peer = floor(random() * connected_nodes)
                while peer == n:
                    peer = floor(random() * connected_nodes)

                if peer in known:
                    next_known.add(n)
            known.update(next_known)

        total_propagation_steps += steps
        minprop = min(minprop, steps)
        maxprop = max(maxprop, steps)

    print(
        f'Iters: {iters}, Nodes: {connected_nodes}, Average propagation steps: {total_propagation_steps / iters:.04f}, Max steps: {maxprop}, Min steps: {minprop}')
