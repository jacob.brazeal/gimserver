config_version=9

# This script only needs to be run once per environment, as it installs nginx.
if [[ "$GIMSERVER_CONFIGURED" == "$config_version" ]]; then
  echo "Environment already configured"
  exit 0 # Already run in the past
fi

# Make directories
mkdir code
mkdir services

linux_version=$(cat /proc/version)

# For AWS Linux
if [[ "$linux_version" == *"amzn"* ]]; then

  # Nginx
  sudo amazon-linux-extras enable nginx1
  sudo yum clean metadata
  sudo yum -y install nginx
  sudo amazon-linux-extras install python3.8 -y
  sudo yum install gcc -y

  sudo amazon-linux-extras install epel -y
  sudo yum install hping3 -y

  sudo systemctl start nginx
  sudo systemctl enable nginx

fi

rm code/loop/gim.db

python3.8 -m pip install --user flask flask-cors requests

echo "GIMSERVER_CONFIGURED=$config_version" | sudo tee -a /etc/environment
source /etc/environment
