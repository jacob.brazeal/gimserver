# Setup: Universal SSH key
mkdir ~/.ssh
cp "$DEPLOY_KEY" ~/.ssh/gimserver
chmod 600 ~/.ssh/gimserver

# Deploy each server
while read usr server name; do
  echo "Setting up $server"

  ssh -o StrictHostKeyChecking=no -i ~/.ssh/gimserver "$usr"@"$server" 'bash -s' < configure.sh

  echo "$server $name" > ../src/name.txt # Pass a name and IP up to the server, so we know who we are when peering
  scp -pr -i ~/.ssh/gimserver ../src/* "$usr"@"$server":/home/"$usr"/code/
  scp -pr -i ~/.ssh/gimserver ../service/* "$usr"@"$server":/home/"$usr"/services/
  rm ../src/name.txt

  ssh -o StrictHostKeyChecking=no -i ~/.ssh/gimserver "$usr"@"$server" 'bash -s' < setup.sh
  ssh -o StrictHostKeyChecking=no -i ~/.ssh/gimserver "$usr"@"$server" 'bash -s' < run-servers.sh

  echo "Finished setting up $server"

done <servers.txt

