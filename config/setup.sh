gcc code/udpecho.c -o udpecho
sudo cp ./udpecho /usr/bin/

sudo cp services/* /etc/systemd/system
sudo chmod +x code/loop/server.sh

sudo systemctl daemon-reload
sudo systemctl restart udpecho

sudo cp code/nginx.conf /etc/nginx/nginx.conf

sudo systemctl restart nginx

sudo touch gim.db
sudo chown ec2-user gim.db # File can be auto-created, so we need to make sure it's ours.
cd code/loop
python3.8 configure_db.py

sudo systemctl enable monloop
sudo systemctl restart monloop

sudo systemctl enable gimserver
sudo systemctl restart gimserver

sudo systemctl enable monpeer
sudo systemctl restart monpeer
